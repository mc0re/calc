﻿namespace CalcLib
{
    public static class CalculatorKeys
    {
        public const char Digit0 = '0';

        public const char Digit1 = '1';

        public const char Digit2 = '2';

        public const char Digit3 = '3';

        public const char Digit4 = '4';

        public const char Digit5 = '5';

        public const char Digit6 = '6';

        public const char Digit7 = '7';

        public const char Digit8 = '8';

        public const char Digit9 = '9';

        public const char Dot = '.';

        public const char Backspace = '<';

        public const char Plus = '+';

        public const char Minus = '-';

        public const char Multiply = '*';

        public const char Divide = '/';

        public const char Equal = '=';

        public const char Clear = 'o';
    }
}
