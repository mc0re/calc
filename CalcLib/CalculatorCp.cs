﻿using System.Linq;

namespace CalcLib
{
    public class CalculatorCp
    {
        #region Constants

        private static readonly char[] Operators = new[]
        {
            CalculatorKeys.Plus,
            CalculatorKeys.Minus,
            CalculatorKeys.Multiply,
            CalculatorKeys.Divide
        };

        #endregion


        #region API

        public string Calculate(string current, char newKey)
        {
            var expr = CalcExpression.Parse(current, Operators);

            switch (newKey)
            {
                case CalculatorKeys.Backspace:
                    return ProcessBackspace(current);

                case CalculatorKeys.Clear:
                    return "";

                case CalculatorKeys.Dot:
                    return ProcessDot(expr, current);

                case CalculatorKeys.Equal:
                    return expr.Calculate();
            }

            if (IsOperator(newKey))
            {
                return ProcessOperator(expr, current, newKey);
            }

            return current + newKey;
        }

        #endregion


        #region Utility

        private static bool IsOperator(char ch)
        {
            return Operators.Contains(ch);
        }


        private static string ProcessBackspace(string current)
        {
            return current.Any() ? current.Substring(0, current.Length - 1) : "";
        }


        private static string ProcessDot(CalcExpression expr, string current)
        {
            if (expr.LeftOperand == "" || expr.RightOperand == "")
                return $"{current}0{CalculatorKeys.Dot}";

            if (!expr.Operator.HasValue)
            {
                if (expr.LeftOperand.Contains(CalculatorKeys.Dot))
                    return current;
                else
                    return current + CalculatorKeys.Dot;
            }
            else
            {
                if (expr.RightOperand.Contains(CalculatorKeys.Dot))
                    return current;
                else
                    return current + CalculatorKeys.Dot;
            }
        }


        private static string ProcessOperator(CalcExpression expr, string current, char newKey)
        {
            // Allow unary minus
            if (expr.LeftOperand == "")
            {
                return newKey == CalculatorKeys.Minus ? $"{CalculatorKeys.Minus}" : "";
            }

            if (expr.RightOperand == null)
            {
                return current + newKey;
            }

            // Replace operator
            if (expr.RightOperand == "")
            {
                return expr.LeftOperand + newKey;
            }

            // Calculate
            return expr.Calculate() + newKey;
        }

        #endregion
    }
}
