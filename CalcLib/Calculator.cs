﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;


namespace CalcLib
{
    public class Calculator
    {
        #region Constants

        private static readonly Dictionary<char, Func<double, double, double>> Operations = new Dictionary<char, Func<double, double, double>>
        {
            { CalculatorKeys.Plus, (a, b) => a + b },
            { CalculatorKeys.Minus, (a, b) => a - b},
            { CalculatorKeys.Multiply, (a, b) => a * b },
            { CalculatorKeys.Divide, (a, b) => b == 0 ? 0 : a / b}
        };

        #endregion


        #region Fields

        private string mKeys = "";

        #endregion


        #region Init and clean-up

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public Calculator()
        {
        }


        /// <summary>
        /// Create a calculator instance with some keys already pressed.
        /// </summary>
        public Calculator(string keys)
        {
            mKeys = keys;
        }

        #endregion


        #region API

        /// <summary>
        /// Indicate to the calculator that a new key is pressed.
        /// </summary>
        /// <param name="key">A digit or one of the special strings (see <see cref="CalculatorKeys"/>)</param>
        /// <returns>New display contents</returns>
        public string KeyPressed(char key)
        {
            switch (key)
            {
                case CalculatorKeys.Backspace:
                    mKeys = DeleteLast(mKeys);
                    break;

                case CalculatorKeys.Clear:
                    mKeys = "";
                    break;

                case CalculatorKeys.Equal:
                    mKeys = CalculateNow(mKeys);
                    break;

                case CalculatorKeys.Plus:
                    mKeys = HandleOperation(mKeys, key, "");
                    break;

                case CalculatorKeys.Minus:
                    mKeys = HandleOperation(mKeys, key, key.ToString());
                    break;

                case CalculatorKeys.Multiply:
                    mKeys = HandleOperation(mKeys, key, "1*");
                    break;

                case CalculatorKeys.Divide:
                    mKeys = HandleOperation(mKeys, key, "1/");
                    break;

                default:
                    if (IsDigit(key))
                        mKeys += key;
                    break;
            }

            return string.IsNullOrEmpty(mKeys) ? "0" : mKeys;
        }

        #endregion


        #region Utility

        private bool IsDigit(char key)
        {
            return char.IsDigit(key) || key == '.';
        }


        private static bool IsLastOperation(string keys)
        {
            return keys.Any() && Operations.ContainsKey(keys.Last());
        }


        private static string DeleteLast(string keys)
        {
            if (string.IsNullOrEmpty(keys))
                return keys;
            else if (IsLastOperation(keys) && keys != CalculatorKeys.Minus.ToString())
                return keys;

            return keys.Substring(0, keys.Length - 1);
        }


        private static string CalculateNow(string keys)
        {
            if (WasOperation(keys, out var operands))
                return Calculate(operands).ToString();
            else
                return keys;
        }


        private static string HandleOperation(
            string keys, char opKey, string startBehaviour)
        {
            // Beginning of the string
            if (string.IsNullOrEmpty(keys))
                return startBehaviour;

            // Replace operation
            else if (IsLastOperation(keys))
                return keys.Substring(0, keys.Length - 1) + opKey;

            // Calculate previuos expression
            else
                return CalculateNow(keys) + opKey;
        }


        private static bool WasOperation(string keys, out string[] operands)
        {
            // Skip unary minus
            for (var idx = 1; idx < keys.Length; idx++)
            {
                if (Operations.ContainsKey(keys[idx]))
                {
                    operands = new[] { keys.Substring(0, idx), keys[idx].ToString(), keys.Substring(idx + 1) };
                    return true;
                }
            }

            operands = null;
            return false;
        }


        private static double Calculate(string[] operands)
        {
            var op1 = double.Parse(operands[0], CultureInfo.InvariantCulture);
            var op2 = double.Parse(operands[2], CultureInfo.InvariantCulture);

            var opFunc = Operations[operands[1].First()];
            return Math.Round(opFunc.Invoke(op1, op2), 3);
        }

        #endregion
    }
}
