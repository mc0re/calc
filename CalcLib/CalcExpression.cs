﻿using System;
using System.Linq;


namespace CalcLib
{
    public class CalcExpression
    {
        #region Properties

        public string LeftOperand { get; set; }

        public string RightOperand { get; set; }

        public char? Operator { get; set; }

        #endregion


        #region Factory API

        public static CalcExpression Parse(string str, char[] operators)
        {
            var operIdx = str.Any() ? str.IndexOfAny(operators, 1) : -1;

            if (operIdx >= 0)
                return new CalcExpression()
                {
                    LeftOperand = str.Substring(0, operIdx),
                    Operator = str[operIdx],
                    RightOperand = str.Substring(operIdx + 1)
                };
            else
                return new CalcExpression()
                {
                    LeftOperand = str
                };
        }

        #endregion


        #region API

        public string Calculate()
        {
            if (string.IsNullOrEmpty(RightOperand))
                return LeftOperand;

            if (!double.TryParse(LeftOperand, out var left) ||
                !double.TryParse(RightOperand, out var right))
            {
                throw new ArgumentException();
            }

            var res = 0.0;

            switch (Operator)
            {
                case CalculatorKeys.Plus:
                    res = left + right;
                    break;

                case CalculatorKeys.Minus:
                    res = left - right;
                    break;

                case CalculatorKeys.Multiply:
                    res = left * right;
                    break;

                case CalculatorKeys.Divide:
                    res = right == 0 ? 0 : left / right;
                    break;
            };

            return res.ToString();
        }

        #endregion
    }
}