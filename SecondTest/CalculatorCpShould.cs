using CalcLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace SecondTest
{
    [TestClass]
    public class CalculatorCpShould
    {
        #region Entering first operand

        [TestMethod]
        public void Calculator_EnterDigit()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("", CalculatorKeys.Digit1);
            Assert.AreEqual("1", result);
        }


        [TestMethod]
        public void Calculator_EnterNextDigit()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12", CalculatorKeys.Digit3);
            Assert.AreEqual("123", result);
        }


        [TestMethod]
        public void Calculator_EnterDot()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("", CalculatorKeys.Dot);
            Assert.AreEqual("0.", result);
        }


        [TestMethod]
        public void Calculator_EnterNextDot()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("1", CalculatorKeys.Dot);
            Assert.AreEqual("1.", result);
        }


        [TestMethod]
        public void Calculator_EnterSecondDot()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("1.2", CalculatorKeys.Dot);
            Assert.AreEqual("1.2", result);
        }

        #endregion


        #region Entering operator

        [TestMethod]
        [DataRow(CalculatorKeys.Plus)]
        [DataRow(CalculatorKeys.Multiply)]
        [DataRow(CalculatorKeys.Divide)]
        public void Calculator_EnterUnaryOperator(char oper)
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("", oper);
            Assert.AreEqual("", result);
        }


        [TestMethod]
        public void Calculator_EnterUnaryMinus()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("", CalculatorKeys.Minus);
            Assert.AreEqual("-", result);
        }


        [TestMethod]
        [DataRow(CalculatorKeys.Plus)]
        [DataRow(CalculatorKeys.Minus)]
        [DataRow(CalculatorKeys.Multiply)]
        [DataRow(CalculatorKeys.Divide)]
        public void Calculator_EnterOperator(char oper)
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12.3", oper);
            Assert.AreEqual("12.3" + oper, result);
        }


        [TestMethod]
        [DataRow(CalculatorKeys.Plus)]
        [DataRow(CalculatorKeys.Minus)]
        [DataRow(CalculatorKeys.Multiply)]
        [DataRow(CalculatorKeys.Divide)]
        public void Calculator_EnterOperatorNegativeOperand(char oper)
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("-12.3", oper);
            Assert.AreEqual("-12.3" + oper, result);
        }


        [TestMethod]
        [DataRow(CalculatorKeys.Plus, CalculatorKeys.Minus)]
        [DataRow(CalculatorKeys.Minus, CalculatorKeys.Multiply)]
        [DataRow(CalculatorKeys.Multiply, CalculatorKeys.Divide)]
        [DataRow(CalculatorKeys.Divide, CalculatorKeys.Plus)]
        public void Calculator_EnterReplaceOperator(char prevOper, char newOper)
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12.3" + prevOper, newOper);
            Assert.AreEqual("12.3" + newOper, result);
        }

        #endregion


        #region Entering second operand

        [TestMethod]
        public void Calculator_EnterDigitInOperand()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12+", CalculatorKeys.Digit3);
            Assert.AreEqual("12+3", result);
        }


        [TestMethod]
        public void Calculator_EnterNextDigitInOperand()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12+3", CalculatorKeys.Digit4);
            Assert.AreEqual("12+34", result);
        }


        [TestMethod]
        public void Calculator_EnterDotAsOperand()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12+", CalculatorKeys.Dot);
            Assert.AreEqual("12+0.", result);
        }


        [TestMethod]
        public void Calculator_EnterDotInOperand()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12+3", CalculatorKeys.Dot);
            Assert.AreEqual("12+3.", result);
        }


        [TestMethod]
        public void Calculator_EnterDotInOperandFirstHasDot()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12.3+4", CalculatorKeys.Dot);
            Assert.AreEqual("12.3+4.", result);
        }


        [TestMethod]
        public void Calculator_EnterSecondDotInOperand()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12+4.5", CalculatorKeys.Dot);
            Assert.AreEqual("12+4.5", result);
        }

        #endregion


        #region Deleting

        [TestMethod]
        public void Calculator_DeleteSecondOperandsDigit()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12+3.4", CalculatorKeys.Backspace);
            Assert.AreEqual("12+3.", result);
        }


        [TestMethod]
        public void Calculator_DeleteSecondOperandsDot()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12+3.", CalculatorKeys.Backspace);
            Assert.AreEqual("12+3", result);
        }


        [TestMethod]
        public void Calculator_DeleteSecondOperand()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12+3", CalculatorKeys.Backspace);
            Assert.AreEqual("12+", result);
        }


        [TestMethod]
        public void Calculator_DeleteOperator()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12+", CalculatorKeys.Backspace);
            Assert.AreEqual("12", result);
        }


        [TestMethod]
        public void Calculator_DeleteFirstOperandsDigit()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12", CalculatorKeys.Backspace);
            Assert.AreEqual("1", result);
        }


        [TestMethod]
        public void Calculator_DeleteFirstOperandsDot()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("1.", CalculatorKeys.Backspace);
            Assert.AreEqual("1", result);
        }


        [TestMethod]
        public void Calculator_DeleteFirstOperand()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("1", CalculatorKeys.Backspace);
            Assert.AreEqual("", result);
        }


        [TestMethod]
        public void Calculator_DeleteWholeExpression()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("12+3.4", CalculatorKeys.Clear);
            Assert.AreEqual("", result);
        }

        #endregion


        #region Calculations

        [TestMethod]
        public void Calculator_CalculateNumber()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("1.2", CalculatorKeys.Equal);
            Assert.AreEqual("1.2", result);
        }


        [TestMethod]
        public void Calculator_CalculateOperator()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("1.2+", CalculatorKeys.Equal);
            Assert.AreEqual("1.2", result);
        }


        [TestMethod]
        public void Calculator_CalculateAdd()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("1.2+3.4", CalculatorKeys.Equal);
            Assert.AreEqual("4.6", result);
        }


        [TestMethod]
        [DataRow(CalculatorKeys.Plus)]
        [DataRow(CalculatorKeys.Minus)]
        [DataRow(CalculatorKeys.Multiply)]
        [DataRow(CalculatorKeys.Divide)]
        public void Calculator_CalculateAddContinue(char oper)
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("1.2+3.4", oper);
            Assert.AreEqual("4.6" + oper, result);
        }


        [TestMethod]
        public void Calculator_CalculateSubtract()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("3.4-1.2", CalculatorKeys.Equal);
            Assert.AreEqual("2.2", result);
        }


        [TestMethod]
        [DataRow(CalculatorKeys.Plus)]
        [DataRow(CalculatorKeys.Minus)]
        [DataRow(CalculatorKeys.Multiply)]
        [DataRow(CalculatorKeys.Divide)]
        public void Calculator_CalculateSubtractContinue(char oper)
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("3.4-1.2", oper);
            Assert.AreEqual("2.2" + oper, result);
        }


        [TestMethod]
        public void Calculator_CalculateMultiply()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("3.4*1.2", CalculatorKeys.Equal);
            Assert.AreEqual("4.08", result);
        }


        [TestMethod]
        [DataRow(CalculatorKeys.Plus)]
        [DataRow(CalculatorKeys.Minus)]
        [DataRow(CalculatorKeys.Multiply)]
        [DataRow(CalculatorKeys.Divide)]
        public void Calculator_CalculateMultiplyContinue(char oper)
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("3.4*1.2", oper);
            Assert.AreEqual("4.08" + oper, result);
        }


        [TestMethod]
        public void Calculator_CalculateDivide()
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("3.6/2.4", CalculatorKeys.Equal);
            Assert.AreEqual("1.5", result);
        }


        [TestMethod]
        [DataRow(CalculatorKeys.Plus)]
        [DataRow(CalculatorKeys.Minus)]
        [DataRow(CalculatorKeys.Multiply)]
        [DataRow(CalculatorKeys.Divide)]
        public void Calculator_CalculateDivideContinue(char oper)
        {
            var sut = new CalculatorCp();
            var result = sut.Calculate("3.6/2.4", oper);
            Assert.AreEqual("1.5" + oper, result);
        }

        #endregion
    }
}
