﻿using System;
using CalcLib;


namespace CalcCommandLine
{
    class Program
    {
        static void Main(string[] args)
        {
            var calc = new Calculator();

            Console.WriteLine("Start typing, ESC to exit.");

            for (; ; )
            {
                var pressed = Console.ReadKey(true);

                if (pressed.Key == ConsoleKey.Escape)
                    break;

                var key = pressed.Key switch
                {
                    ConsoleKey.Enter => CalculatorKeys.Equal,
                    ConsoleKey.Spacebar => CalculatorKeys.Clear,
                    ConsoleKey.Delete => CalculatorKeys.Clear,
                    ConsoleKey.Backspace => CalculatorKeys.Backspace,
                    _ => pressed.KeyChar
                };

                Console.Write("\r" + new string(' ', Console.BufferWidth) + "\r");

                var display = calc.KeyPressed(key);
                Console.Write(display);
            }
        }
    }
}
