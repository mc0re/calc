# calc

Simple calculator as console and a WPF application.

# Visual Studio 2019

The installer was created by this command:

```
./vs_community.exe --layout C:\VS2019-fw `
	--add Microsoft.VisualStudio.Workload.CoreEditor `
	--add Microsoft.VisualStudio.Component.IntelliCode `
	--add Microsoft.VisualStudio.Component.NuGet `
	--add Microsoft.NetCore.Component.SDK `
	--add Microsoft.NetCore.Component.DevelopmentTools `
	--add Microsoft.NetCore.Component.Web `
	--add Microsoft.VisualStudio.Workload.ManagedDesktop `
	--add Microsoft.Net.Component.4.8.SDK `
	--add Microsoft.Net.Component.4.8.TargetingPack `
	--add Microsoft.VisualStudio.Component.Git `
	--lang en-US
```
