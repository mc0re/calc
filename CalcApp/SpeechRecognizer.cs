﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Recognition;
using System.Windows.Controls;
using System.Windows.Input;


namespace CalcApp
{
    public sealed class SpeechRecognizer : IDisposable
    {
        #region Fields

        private readonly SpeechRecognitionEngine mSpeechRecognizer = new SpeechRecognitionEngine();


        private readonly Control mTarget;


        private static readonly Dictionary<string, CommandDescription> CommandList = new Dictionary<string, CommandDescription>
        {
            {"zero", new CommandDescription("NumberCommand", "0") },
            {"one", new CommandDescription("NumberCommand", "1") },
            {"two", new CommandDescription("NumberCommand", "2") },
            {"three", new CommandDescription("NumberCommand", "3") },
            {"four", new CommandDescription("NumberCommand", "4") },
            {"five", new CommandDescription("NumberCommand", "5") },
            {"six", new CommandDescription("NumberCommand", "6") },
            {"seven", new CommandDescription("NumberCommand", "7") },
            {"eight", new CommandDescription("NumberCommand", "8") },
            {"nine", new CommandDescription("NumberCommand", "9") },
            {"ten", new CommandDescription("NumberCommand", "1", "0") },
            {"eleven", new CommandDescription("NumberCommand", "1", "1") },
            {"twelve", new CommandDescription("NumberCommand", "1", "2") },
            {"thirteen", new CommandDescription("NumberCommand", "1", "3") },
            {"fourteen", new CommandDescription("NumberCommand", "1", "4") },
            {"fifteen", new CommandDescription("NumberCommand", "1", "5") },
            {"sixteen", new CommandDescription("NumberCommand", "1", "6") },
            {"seventeen", new CommandDescription("NumberCommand", "1", "7") },
            {"eighteen", new CommandDescription("NumberCommand", "1", "8") },
            {"nineteen", new CommandDescription("NumberCommand", "1", "9") },
            {"twenty", new CommandDescription("NumberCommand", "2", "0") },
            {"comma", new CommandDescription("DotCommand") },
            {"delete", new CommandDescription("BackspaceCommand") },
            {"clear", new CommandDescription("ClearCommand") },
            {"add", new CommandDescription("OperationCommand", "+") },
            {"and", new CommandDescription("OperationCommand", "+") },
            {"plus", new CommandDescription("OperationCommand", "+") },
            {"minus", new CommandDescription("OperationCommand", "-") },
            {"times", new CommandDescription("OperationCommand", "*") },
            {"divide", new CommandDescription("OperationCommand", "/") },
            {"is", new CommandDescription("ResultCommand") }
        };

        #endregion


        #region Init and clean-up

        public SpeechRecognizer(Control target)
        {
            mSpeechRecognizer.AudioStateChanged += AudioStateChangedHanlder;
            mSpeechRecognizer.SpeechRecognized += OnSpeechRecognized;
            mSpeechRecognizer.SpeechRecognitionRejected += OnSpeechRejected;
            mTarget = target;
        }


        public void Dispose()
        {
            mSpeechRecognizer.Dispose();
        }

        #endregion


        #region Recognition API

        /// <summary>
        /// Collect command list for voice recognition from mVoiceSettingList to mVoiceControls.
        /// </summary>
        /// <returns>False if no commands, True if commands are set up, and the engine can be started</returns>
        public bool StartListening()
        {
            try
            {
                var hasGr = mSpeechRecognizer.Grammars.Any();
                var gr = PrepareGrammar();
                if (gr == null)
                    return false;

                mSpeechRecognizer.RequestRecognizerUpdate();
                mSpeechRecognizer.UnloadAllGrammars();
                mSpeechRecognizer.LoadGrammarAsync(gr);
                mSpeechRecognizer.SetInputToDefaultAudioDevice();

                if (!hasGr)
                {
                    mSpeechRecognizer.RecognizeAsync(RecognizeMode.Multiple);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        /// <summary>
        /// Stop listening and recognition.
        /// </summary>
        public void StopListening()
        {
            mSpeechRecognizer.RecognizeAsyncStop();
        }

        #endregion


        #region Recognition utility

        /// <summary>
        /// Create grammar for recognition.
        /// </summary>
        private Grammar PrepareGrammar()
        {
            var commandChoices = new Choices();

            // Create grammar
            foreach (var cmdInfo in CommandList)
            {
                var rCmd = mTarget.FindResource(cmdInfo.Value.CommandName) as RoutedCommand;
                if (rCmd is null)
                    throw new ArgumentException($"Unknown command '{cmdInfo.Value.CommandName}'.");

                cmdInfo.Value.Command = rCmd;
                commandChoices.Add(cmdInfo.Key);
            }

            try
            {
                var gb = new GrammarBuilder(commandChoices);
                return new Grammar(gb);
            }
            catch (Exception)
            {
                return null;
            }
        }


        /// <summary>
        /// Triggered when engine is stopped.
        /// </summary>
        private void AudioStateChangedHanlder(object sender, AudioStateChangedEventArgs args)
        {
        }


        /// <summary>
        /// Command is recognized, execute.
        /// </summary>
        private void OnSpeechRecognized(object sender, SpeechRecognizedEventArgs args)
        {
            var cmdText = string.Join(" ", from w in args.Result.Words select w.Text);

            Console.WriteLine(cmdText);

            if (!CommandList.TryGetValue(cmdText, out var cmd))
            {
                return;
            }

            if (!cmd.Parameters.Any())
            {
                cmd.Command.Execute(null, mTarget);
            }

            foreach (var param in cmd.Parameters)
            {
                cmd.Command.Execute(param, mTarget);
            }
        }


        /// <summary>
        /// Unrecognized command.
        /// </summary>
        private void OnSpeechRejected(object sender, SpeechRecognitionRejectedEventArgs args)
        {
        }

        #endregion
    }
}
