﻿using System;
using System.Windows;
using System.Windows.Input;
using CalcLib;

namespace CalcApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields

        private Calculator mCalculator;

        private SpeechRecognizer mSpeechEngine;

        #endregion


        #region MainDisplay read-only dependency property

        private static readonly DependencyPropertyKey MainDisplayPropertyKey = DependencyProperty.RegisterReadOnly(
            nameof(MainDisplay), typeof(string), typeof(MainWindow),
            new PropertyMetadata("0"));


        public static readonly DependencyProperty MainDisplayProperty = MainDisplayPropertyKey.DependencyProperty;


        /// <summary>
        /// The text shown on the calculator's display.
        /// </summary>
        public string MainDisplay
        {
            get => (string)GetValue(MainDisplayProperty);
            private set => SetValue(MainDisplayPropertyKey, value);
        }

        #endregion


        #region Init and clean-up

        public MainWindow()
        {
            InitializeComponent();

            mCalculator = new Calculator();
        }


        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            mSpeechEngine = new SpeechRecognizer(this);
        }


        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);
            mSpeechEngine.StartListening();
        }


        protected override void OnClosed(EventArgs e)
        {
            mSpeechEngine.StopListening();
            base.OnClosed(e);
        }

        #endregion


        #region UI handlers

        private void NumberCommandExecuted(object sender, ExecutedRoutedEventArgs args)
        {
            var param = (args.Parameter as string)[0];
            MainDisplay = mCalculator.KeyPressed(param);
        }


        private void DotCommandExecuted(object sender, ExecutedRoutedEventArgs args)
        {
            MainDisplay = mCalculator.KeyPressed('.');
        }


        private void BackspaceCommandExecuted(object sender, ExecutedRoutedEventArgs args)
        {
            MainDisplay = mCalculator.KeyPressed(CalculatorKeys.Backspace);
        }


        private void ClearCommandExecuted(object sender, ExecutedRoutedEventArgs args)
        {
            MainDisplay = mCalculator.KeyPressed(CalculatorKeys.Clear);
        }


        private void OperationCommandExecuted(object sender, ExecutedRoutedEventArgs args)
        {
            var param = (args.Parameter as string)[0];
            MainDisplay = mCalculator.KeyPressed(param);
        }


        private void ResultCommandExecuted(object sender, ExecutedRoutedEventArgs args)
        {
            MainDisplay = mCalculator.KeyPressed(CalculatorKeys.Equal);
        }

        #endregion
    }
}
