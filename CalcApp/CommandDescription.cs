﻿using System.Windows.Input;

namespace CalcApp
{
    public class CommandDescription
    {
        #region Properties

        public string CommandName { get; }

        public string[] Parameters { get; }

        public RoutedCommand Command { get; internal set; }

        #endregion


        #region Init and clean-up

        public CommandDescription(string commandName, params string[] parameters)
        {
            CommandName = commandName;
            Parameters = parameters;
        }

        #endregion
    }
}