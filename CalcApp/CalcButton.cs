﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace CalcApp
{
    /// <summary>
    /// Button with properties for animations.
    /// </summary>
    public class CalcButton : Button
    {
        #region MouseOverBackground dependency property

        public static readonly DependencyProperty MouseOverBackgroundProperty = DependencyProperty.Register(
            nameof(MouseOverBackground), typeof(Brush), typeof(CalcButton),
            new PropertyMetadata(Brushes.Transparent));


        public Brush MouseOverBackground
        {
            get => (Brush)GetValue(MouseOverBackgroundProperty);
            set => SetValue(MouseOverBackgroundProperty, value);
        }

        #endregion


        #region MouseOverOpacity dependency property

        public static readonly DependencyProperty MouseOverOpacityProperty = DependencyProperty.Register(
            nameof(MouseOverOpacity), typeof(double), typeof(CalcButton),
            new PropertyMetadata(0.0));


        public double MouseOverOpacity
        {
            get => (double)GetValue(MouseOverOpacityProperty);
            set => SetValue(MouseOverOpacityProperty, value);
        }

        #endregion
    }
}
