using CalcLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace CalcLibTest
{
    [TestClass]
    public class CalculatorShould
    {
        #region Enter tests

        [TestMethod]
        public void Calculator_EnterDigis()
        {
            var sut = new Calculator();

            Assert.AreEqual("1", sut.KeyPressed('1'));
            Assert.AreEqual("12", sut.KeyPressed('2'));
            Assert.AreEqual("12.", sut.KeyPressed('.'));
            Assert.AreEqual("12.0", sut.KeyPressed('0'));
        }


        [TestMethod]
        public void Calculator_EnterPlus()
        {
            var sut = new Calculator("1");
            Assert.AreEqual("1+", sut.KeyPressed('+'));
        }


        [TestMethod]
        public void Calculator_EnterMinus()
        {
            var sut = new Calculator("12.5");
            Assert.AreEqual("12.5-", sut.KeyPressed('-'));
        }


        [TestMethod]
        public void Calculator_EnterMultiplication()
        {
            var sut = new Calculator("12.5");
            Assert.AreEqual("12.5*", sut.KeyPressed('*'));
        }


        [TestMethod]
        public void Calculator_EnterDivision()
        {
            var sut = new Calculator("12.5");
            Assert.AreEqual("12.5/", sut.KeyPressed('/'));
        }


        [TestMethod]
        public void Calculator_StartWithPlus()
        {
            var sut = new Calculator();
            Assert.AreEqual("0", sut.KeyPressed('+'));
        }


        [TestMethod]
        public void Calculator_StartWithMinus()
        {
            var sut = new Calculator();
            Assert.AreEqual("-", sut.KeyPressed('-'));
        }


        [TestMethod]
        public void Calculator_StartWithMultiplication()
        {
            var sut = new Calculator();
            Assert.AreEqual("1*", sut.KeyPressed('*'));
        }


        [TestMethod]
        public void Calculator_StartWithDivision()
        {
            var sut = new Calculator();
            Assert.AreEqual("1/", sut.KeyPressed('/'));
        }


        [TestMethod]
        public void Calculator_ReplaceOperationMyPlus()
        {
            var sut = new Calculator("1*");
            Assert.AreEqual("1+", sut.KeyPressed('+'));
        }


        [TestMethod]
        public void Calculator_ReplaceOperationByMinus()
        {
            var sut = new Calculator("1+");
            Assert.AreEqual("1-", sut.KeyPressed('-'));
        }


        [TestMethod]
        public void Calculator_ReplaceOperationByMultiply()
        {
            var sut = new Calculator("1+");
            Assert.AreEqual("1*", sut.KeyPressed('*'));
        }


        [TestMethod]
        public void Calculator_ReplaceOperationByDivide()
        {
            var sut = new Calculator("1+");
            Assert.AreEqual("1/", sut.KeyPressed('/'));
        }

        #endregion


        #region Calculation tests

        [TestMethod]
        public void Calculator_PlusGivesResult()
        {
            var sut = new Calculator("1+2");
            Assert.AreEqual("3*", sut.KeyPressed('*'));
        }


        [TestMethod]
        public void Calculator_MinusGivesResult()
        {
            var sut = new Calculator("1-2");
            Assert.AreEqual("-1*", sut.KeyPressed('*'));
        }


        [TestMethod]
        public void Calculator_MultiplicationGivesResult()
        {
            var sut = new Calculator("2*3");
            Assert.AreEqual("6*", sut.KeyPressed('*'));
        }


        [TestMethod]
        public void Calculator_DivisionGivesResult()
        {
            var sut = new Calculator("3/2");
            Assert.AreEqual("1.5*", sut.KeyPressed('*'));
        }

        #endregion


        #region Correction tests

        [TestMethod]
        public void Calculator_DeleteDigit()
        {
            var sut = new Calculator("10");
            Assert.AreEqual("1", sut.KeyPressed('<'));
        }


        [TestMethod]
        public void Calculator_DoNotDeleteZero()
        {
            var sut = new Calculator("0");
            Assert.AreEqual("0", sut.KeyPressed('<'));
        }


        [TestMethod]
        public void Calculator_DeleteNumberAfterOperation()
        {
            var sut = new Calculator("1+2");
            Assert.AreEqual("1+", sut.KeyPressed('<'));
        }


        [TestMethod]
        public void Calculator_DoNotDeleteOperation()
        {
            var sut = new Calculator("1+");
            Assert.AreEqual("1+", sut.KeyPressed('<'));
        }


        [TestMethod]
        public void Calculator_DeleteFirstMinus()
        {
            var sut = new Calculator("-");
            Assert.AreEqual("0", sut.KeyPressed('<'));
        }


        [TestMethod]
        public void Calculator_DeleteEmpty()
        {
            var sut = new Calculator();
            Assert.AreEqual("0", sut.KeyPressed('<'));
        }

        #endregion
    }
}
